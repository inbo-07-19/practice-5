from calculator import calc

def test_calc():
    assert calc(100, 'мм', 'мм') == 100.0
    assert calc(100, 'мм', 'см') == 10.0
    assert calc(100, 'мм', 'м') == 0.1
    assert calc(100, 'мм', 'дм') == 1.0
    assert calc(100, 'мм', 'км') == 0.0001
    
    assert calc(100, 'см', 'мм') == 1000.0
    assert calc(100, 'см', 'см') == 100.0
    assert calc(100, 'см', 'м') == 1.0
    assert calc(100, 'см', 'дм') == 10.0
    assert calc(100, 'см', 'км') == 0.001
    
    assert calc(1, 'дм', 'мм') == 100.0
    assert calc(1, 'дм', 'см') == 10.0
    assert calc(1, 'дм', 'м') == 0.1
    assert calc(1, 'дм', 'дм') == 1.0
    assert calc(1, 'дм', 'км') == 0.0001
    
    assert calc(1, 'м', 'мм') == 1000.0
    assert calc(1, 'м', 'см') == 100.0
    assert calc(1, 'м', 'м') == 1.0
    assert calc(1, 'м', 'дм') == 10.0
    assert calc(1, 'м', 'км') == 0.001
    
    assert calc(1, 'км', 'мм') == 1000000.0
    assert calc(1, 'км', 'см') == 100000.0
    assert calc(1, 'км', 'м') == 1000.0
    assert calc(1, 'км', 'дм') == 10000.0
    assert calc(1, 'км', 'км') == 1
